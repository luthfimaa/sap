import os
import pandas as pd

def read(path):
  files = [f for f in os.listdir(path) if '.txt' in f]
  strings = [open(os.path.join(path, f)).read() for f in files]
  return strings

def to_csv(path, label):
  text = read(path)
  labels = [label] * len(text)
  return pd.DataFrame({'text': text, 'label': labels})

def save_df(df, path, name):
  df.to_csv(os.path.join(path, name), index=False)

pos_train = to_csv('data/train/pos', 'pos')
neg_train = to_csv('data/train/neg', 'neg')
train_df = pd.concat([pos_train, neg_train])
train_df = train_df.sample(frac=1)
train_df = train_df[['text', 'label']]

pos_test = to_csv('data/test/pos', 'pos')
neg_test = to_csv('data/test/neg', 'neg')
test_df = pd.concat([pos_test, neg_test])
test_df = test_df.sample(frac=1)
test_df = test_df[['text', 'label']].reset_index(drop=True)

dev_ratio = 0.2
size = int(len(train_df) * (1-dev_ratio))
train_df = train_df.iloc[:size].reset_index(drop=True)
dev_df = train_df.iloc[size:].reset_index(drop=True)

save_df(train_df, 'pdata', 'train.csv')
save_df(dev_df, 'pdata', 'dev.csv')
save_df(test_df, 'pdata', 'test.csv')
