import torch
import torch.nn as nn
import torchtext
import torchtext.data as data
import numpy as np
import logging
import os
from tqdm import *

from sap.model import LSTM
from sap.utils import *

def get_fields():
    def label_to_tensor(label):
        return [1] if label == 'pos' else [0]

    text = data.ReversibleField(tokenize='spacy',
                      tensor_type=torch.LongTensor,
                      use_vocab=True,
                      include_lengths=True,
                      pad_first=True)

    label = data.Field(sequential=False,
                       tensor_type=torch.LongTensor,
                       preprocessing=label_to_tensor,
                       use_vocab=False)
    return text, label

def get_datasets(root, path=('train.csv', 'dev.csv', 'test.csv'), cache='output/cache'):
    text, label = get_fields()
    fields = [('text', text), ('label', label)]
    train, dev, test = data.TabularDataset.splits(path=root,
                                                  root='.',
                                                  train=path[0],
                                                  validation=path[1],
                                                  test=path[2],
                                                  fields=fields,
                                                  format='csv')
    text.build_vocab(train, dev, test)
    dump_vocab(text.vocab, root=cache)
    return train, dev, test

def get_iterators(datasets, batch_sizes=(256, 256, 256)):
    """
    datasets: Tuple(train, dev, test)
    """
    device = None if torch.cuda.is_available() else -1
    train_it, dev_it, test_it = data.BucketIterator.splits(
            datasets,
            batch_sizes,
            device=device,
            sort_key=lambda x: len(x.text),
            sort_within_batch=True)
    return train_it, dev_it, test_it

def train(model, it, epochs=100, root='output', path='model.pth'):
    logger = get_logger('train')
    criterion = nn.NLLLoss()
    optimizer = torch.optim.Adam(model.parameters())
    for epoch in tqdm(range(epochs)):
        losses = []
        for batch in it:
            optimizer.zero_grad()
            (x, lengths) = batch.text
            model.hidden = model.init_hidden()

            out = model(x, lengths)
            y = batch.label.squeeze(1)

            loss = criterion(out, y)
            optimizer.step()

            losses.append(float(loss.data[0]))
        logger.debug('[epoch %d loss: %f]' % (epoch, np.average(losses)))
        torch.save(model, os.path.join(root, path))
