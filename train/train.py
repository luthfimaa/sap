import logging
from train.common import *
from sap.model import LSTM
from sap.utils import *

CACHE = '/output/cache'
DATAPATH = '/data'

logger = get_logger('train.py')
logger.debug('preparing dataset...')

datasets = get_datasets(DATAPATH, cache=CACHE)
train_it, dev_it, test_it = get_iterators(datasets)
vocab = load_vocab(root=CACHE)
text = get_field(vocab)

model = LSTM(300, 32, len(text.vocab.itos), 2)
# model.train()
if torch.cuda.is_available():
    model.cuda()
logger.debug('start training...')
train(model, train_it, root=CACHE)
train(model, test_it, root=CACHE)
