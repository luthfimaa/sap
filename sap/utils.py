import logging
import torch
import torchtext.data as data
import pickle
import os

def dump_vocab(vocab, root='.cache', path='vocab.pkl'):
    if not os.path.isdir(root):
        os.mkdir(root)
    with open(os.path.join(root, path), 'wb+') as f:
        pickle.dump(vocab, f)
        
def load_vocab(root='.cache', path='vocab.pkl'):
    with open(os.path.join(root, path), 'rb') as f:
        vocab = pickle.load(f)
    return vocab

def get_field(vocab):
    text = data.ReversibleField(tokenize='spacy', tensor_type=torch.LongTensor, use_vocab=True, include_lengths=True)
    text.vocab = vocab
    return text

def to_batch(inputs, fields):
    examples = [data.example.Example.fromlist([t], fields) for t in inputs]
    d = data.Dataset(examples, fields)
    it = data.Iterator(d, len(inputs), sort_key=lambda x: len(x.text), train=False, sort=False, device=-1)
    batch = next(iter(it))
    return batch

def get_logger(name):
    logging.basicConfig(
            format="%(levelname)-4s %(asctime)s %(message)s",
            level=logging.DEBUG
            )
    logger = logging.getLogger(name)
    return logger
