import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.autograd as autograd

import sap.utils as utils

class LSTM(nn.Module):
    def __init__(self, embedding_dim, hidden_dim, vocab_size, label_size):
        super(LSTM, self).__init__()
        self.hidden_dim = hidden_dim

        self.word_embeddings = nn.Embedding(vocab_size, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim, hidden_dim)

        self.fc1 = nn.Linear(hidden_dim, hidden_dim)
        self.fc2 = nn.Linear(hidden_dim, label_size)

        self.d1 = nn.Dropout()

        self.hidden = self.init_hidden()

    def init_hidden(self):
        return (autograd.Variable(torch.zeros(1, 1, self.hidden_dim)),
                autograd.Variable(torch.zeros(1, 1, self.hidden_dim)))
    
    def forward(self, input, lengths):
        embeds = self.word_embeddings(input)
        packed =  nn.utils.rnn.pack_padded_sequence(embeds, list(lengths))
        out, self.hidden = self.lstm(packed, self.hidden)
        out, _ = nn.utils.rnn.pad_packed_sequence(out)
        out = out[-1, :, :]

        out = self.d1(self.fc1(out))
        out = self.fc2(out)

        out = F.log_softmax(out, dim=1)
        return out

    def predict(self, strings):
        fields = [('text', get_field(load_vocab()))]
        batch = utils.to_batch(strings, fields)
        tensor, lengths = batch.text
        return self.forward(tensor, lengths)

# def load(name='lstm'):
#     models = {
#             'lstm': LSTM()
#             }

#     model = models[name]
#     # load embedding and vocab
